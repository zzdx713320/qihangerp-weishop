package cn.qihangerp.api.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.qihangerp.api.domain.ErpGoodsCategoryAttribute;
import cn.qihangerp.api.service.ErpGoodsCategoryAttributeService;
import cn.qihangerp.api.mapper.ErpGoodsCategoryAttributeMapper;
import org.springframework.stereotype.Service;

/**
* @author TW
* @description 针对表【erp_goods_category_attribute】的数据库操作Service实现
* @createDate 2024-04-12 15:29:36
*/
@Service
public class ErpGoodsCategoryAttributeServiceImpl extends ServiceImpl<ErpGoodsCategoryAttributeMapper, ErpGoodsCategoryAttribute>
    implements ErpGoodsCategoryAttributeService{

}




