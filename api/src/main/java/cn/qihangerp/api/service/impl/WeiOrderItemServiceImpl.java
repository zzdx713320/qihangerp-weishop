package cn.qihangerp.api.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.qihangerp.api.domain.WeiOrderItem;
import cn.qihangerp.api.service.WeiOrderItemService;
import cn.qihangerp.api.mapper.WeiOrderItemMapper;
import org.springframework.stereotype.Service;

/**
* @author qilip
* @description 针对表【wei_order_item】的数据库操作Service实现
* @createDate 2024-04-03 21:36:32
*/
@Service
public class WeiOrderItemServiceImpl extends ServiceImpl<WeiOrderItemMapper, WeiOrderItem>
    implements WeiOrderItemService{

}




