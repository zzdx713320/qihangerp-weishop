package cn.qihangerp.api.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.qihangerp.api.domain.WeiGoodsSku;
import cn.qihangerp.api.service.WeiGoodsSkuService;
import cn.qihangerp.api.mapper.WeiGoodsSkuMapper;
import org.springframework.stereotype.Service;

/**
* @author qilip
* @description 针对表【wei_goods_sku】的数据库操作Service实现
* @createDate 2024-04-03 21:36:32
*/
@Service
public class WeiGoodsSkuServiceImpl extends ServiceImpl<WeiGoodsSkuMapper, WeiGoodsSku>
    implements WeiGoodsSkuService{

}




