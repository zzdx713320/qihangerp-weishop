package cn.qihangerp.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.qihangerp.api.domain.SysPlatform;


/**
* @author TW
* @description 针对表【sys_platform】的数据库操作Service
* @createDate 2024-03-21 13:49:22
*/
public interface SysPlatformService extends IService<SysPlatform> {

}
