package cn.qihangerp.api.mapper;

import cn.qihangerp.api.domain.WeiGoodsSku;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author qilip
* @description 针对表【wei_goods_sku】的数据库操作Mapper
* @createDate 2024-04-03 21:36:32
* @Entity cn.qihangerp.api.domain.WeiGoodsSku
*/
public interface WeiGoodsSkuMapper extends BaseMapper<WeiGoodsSku> {

}




