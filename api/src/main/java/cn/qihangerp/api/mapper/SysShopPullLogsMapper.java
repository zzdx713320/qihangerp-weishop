package cn.qihangerp.api.mapper;

import cn.qihangerp.api.domain.SysShopPullLogs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author qilip
* @description 针对表【sys_shop_pull_logs(更新日志表)】的数据库操作Mapper
* @createDate 2024-04-04 17:50:02
* @Entity cn.qihangerp.api.domain.SysShopPullLogs
*/
public interface SysShopPullLogsMapper extends BaseMapper<SysShopPullLogs> {

}




