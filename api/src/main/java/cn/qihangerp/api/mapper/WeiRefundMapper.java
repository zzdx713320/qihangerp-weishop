package cn.qihangerp.api.mapper;

import cn.qihangerp.api.domain.WeiRefund;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author qilip
* @description 针对表【wei_refund(视频号小店退款)】的数据库操作Mapper
* @createDate 2024-04-04 23:11:44
* @Entity cn.qihangerp.api.domain.WeiRefund
*/
public interface WeiRefundMapper extends BaseMapper<WeiRefund> {

}




