package cn.qihangerp.api.mapper;

import cn.qihangerp.api.domain.ErpOrderItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author TW
* @description 针对表【erp_order_item(订单明细表)】的数据库操作Mapper
* @createDate 2024-04-03 15:57:41
* @Entity cn.qihangerp.api.domain.ErpOrderItem
*/
public interface ErpOrderItemMapper extends BaseMapper<ErpOrderItem> {
//    PageResult<ErpOrderItemListVo> selectOrderItemList(ErpOrderItemListBo bo);
}




