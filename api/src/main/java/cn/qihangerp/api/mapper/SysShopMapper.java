package cn.qihangerp.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.qihangerp.api.domain.SysShop;


/**
* @author qilip
* @description 针对表【sys_shop(数据中心-店铺)】的数据库操作Mapper
* @createDate 2024-03-17 15:17:34
* @Entity com.qihang.oms.domain.SysShop
*/
public interface SysShopMapper extends BaseMapper<SysShop> {

}




