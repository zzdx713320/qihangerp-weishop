package cn.qihangerp.api.mapper;

import cn.qihangerp.api.domain.ErpGoodsCategoryAttribute;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author TW
* @description 针对表【erp_goods_category_attribute】的数据库操作Mapper
* @createDate 2024-04-12 15:29:36
* @Entity cn.qihangerp.api.domain.ErpGoodsCategoryAttribute
*/
public interface ErpGoodsCategoryAttributeMapper extends BaseMapper<ErpGoodsCategoryAttribute> {

}




