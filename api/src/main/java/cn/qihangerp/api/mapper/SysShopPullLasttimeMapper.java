package cn.qihangerp.api.mapper;

import cn.qihangerp.api.domain.SysShopPullLasttime;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author qilip
* @description 针对表【sys_shop_pull_lasttime(店铺更新最后时间记录)】的数据库操作Mapper
* @createDate 2024-04-04 17:50:02
* @Entity cn.qihangerp.api.domain.SysShopPullLasttime
*/
public interface SysShopPullLasttimeMapper extends BaseMapper<SysShopPullLasttime> {

}




