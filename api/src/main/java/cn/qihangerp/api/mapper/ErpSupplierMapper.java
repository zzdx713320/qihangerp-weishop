package cn.qihangerp.api.mapper;

import cn.qihangerp.api.domain.ErpSupplier;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author TW
* @description 针对表【erp_supplier】的数据库操作Mapper
* @createDate 2024-04-12 15:07:31
* @Entity cn.qihangerp.api.domain.ErpSupplier
*/
public interface ErpSupplierMapper extends BaseMapper<ErpSupplier> {

}




