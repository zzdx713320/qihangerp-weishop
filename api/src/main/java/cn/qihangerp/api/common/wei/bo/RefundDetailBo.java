package cn.qihangerp.api.common.wei.bo;

import lombok.Data;

@Data
public class RefundDetailBo {
    private String after_sale_order_id;
}
