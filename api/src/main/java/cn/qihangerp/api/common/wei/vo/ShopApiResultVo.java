package cn.qihangerp.api.common.wei.vo;

import lombok.Data;

@Data
public class ShopApiResultVo extends BaseResVo{
    private ShopInfoVo info;
}
