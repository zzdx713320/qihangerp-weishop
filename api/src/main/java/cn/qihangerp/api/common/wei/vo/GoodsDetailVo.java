package cn.qihangerp.api.common.wei.vo;

import lombok.Data;

@Data
public class GoodsDetailVo extends BaseResVo {

    private ProductVo product;

}
