package cn.qihangerp.api.common.wei.bo;

import lombok.Data;

@Data
public class OrderDetailBo {
    private String order_id;
}
