package cn.qihangerp.api.common.wei.vo;

import lombok.Data;

@Data
public class RefundDetailVo extends BaseResVo {

    private AfterSaleOrderVo after_sale_order;

}
